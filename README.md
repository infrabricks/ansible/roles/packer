# Packer
Ansible role to install packer.

## Requirements

* Ansible >= 4

## OS

* Debian

## Role Variables

Variable name              | Variable description         | Type    | Default |
---                        | ---                          | ---     | ---     |
packer_local_repokeys_path | Local path of repos pubkeys  | string  | Yes     |
packer_repo_url            | Remote Hashicorp repo URL    | string  | Yes     |
packer_repo_pubkey         | Hashicorp repo pubkey name   | string  | Yes     |
packer_repo_keyring        | Hashicorp repo keyring name  | string  | Yes     |
packer_packages            | Packages list to install     | string  | Yes     |

## Dependencies

No.

## Playbook Example

```
- name: Install Packer
  hosts: all
  tasks:
    - name: Include Packer role
      ansible.builtin.include_role:
        name: packer

```

## License

GPL v3

## Author Information

* [Stephane Paillet](mailto:spaillet@ethicsys.fr)